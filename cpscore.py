import re, subprocess, base64, os, json, logging, pkgutil, argparse, datetime, time
logger = logging.getLogger(__name__)
import yaml
import jinja2
# FIXME: should replace FileSystemLoader with PackageLoader
# or use pkg_resources to find the directory we care about
jinjaenv = jinja2.Environment(loader=jinja2.PackageLoader(__name__),
        autoescape=jinja2.select_autoescape(enabled_extensions=('html','htm','xml'),
            default_for_string=True)
        )

import sys
if getattr(sys, 'frozen', False):
        # we are running in a bundle
        bundle_dir = sys._MEIPASS
else:
        # we are running in a normal Python environment
        bundle_dir = os.path.dirname(os.path.abspath(__file__))

parser = argparse.ArgumentParser()
parser.add_argument('--verbose','-v',action="count",default=0)
parser.add_argument('--run-once',action="store_true",default=False)
parser.add_argument('--config',default="c:\Program Files\cpscore\config.yml")
args = parser.parse_args()

if args.verbose>1:
    logging.basicConfig(level=logging.DEBUG)
elif args.verbose>0:
    logging.basicConfig(level=logging.INFO)

config = yaml.safe_load(open(args.config,"r"))

logger.debug("config: %s",repr(config))
#print(os.getcwd())

while True:
    totalpossiblepoints = 0
    totalpoints = 0
    totalchecks = 0
    results = []

    print("Running Checks...")

    for check in config['checks']:
        if 'name' not in check:
            logger.warning("check missing name!")
            continue
        if 'type' not in check:
            logger.warning("check missing type!")
            continue

        points = int(check.get('points','1'))
        totalpossiblepoints += points
        totalchecks += 1
        try:
            if check['type'] == "powershell" and 'script' in check:
                execresult = subprocess.run(
                    ["C:\\WINDOWS\\system32\\WindowsPowerShell\\v1.0\\powershell.exe","-encodedCommand",
                    base64.b64encode(check['script'].encode('utf-16le')).decode('utf-8')],
                    stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                #print("run compete")
                output = execresult.stdout.decode('utf-8')
            elif check['type'] == "bash" and 'script' in check:
                execresult = subprocess.run(
                    ["/bin/bash","-c",check['script']],
                    stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                #print("run compete")
                output = execresult.stdout.decode('utf-8')
            else:
                raise ValueError("unknown type",check['type'])
            #print(output)
            #print("stderr",execresult.stderr)
            if execresult.returncode != 0:
                message = "non-zero exit"
                result = False
            else:
                if 'regex' in check:
                    mo = re.search(check['regex'],output, re.MULTILINE)
                    if 'validator' in check and mo is not None:
                        result = eval(check['validator'],{'os':os,'re':re,'json':json},{'match':mo})
                        message = "validator check"
                    else:
                        result = mo is not None
                        message = "regex compare"
                else:
                    result = True
                    message = "script passed"
                if 'invert' in check:
                    result = not result

            if result:
                totalpoints += points
                results.append({"name":check['name'],
                    "points":points,
                    "message":message})
        except:
            logger.exception("failed on check %s",check['name'])

    if 'output' in config:
        with open(config['output'],"w") as fo:
            template = jinjaenv.from_string(open(os.path.join(bundle_dir,"templates","score.html"),"r").read())
            fo.write(template.render(\
                checkdatetime=datetime.datetime.now(),
                totalchecks=totalchecks,
                totalpoints=totalpoints,
                totalpossiblepoints=totalpossiblepoints,
                results=results))
    else:
        for r in result:
            print(repr(r))

    if args.run_once:
        break
    else:
        time.sleep(60)
