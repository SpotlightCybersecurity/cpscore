from setuptools import setup

setup( name="cpscore",
    version="0.0.2",
    author="Ryan",
    author_email="ryan@spotlightcybersecurity.com",
    license='MIT',
    py_modules=['cpscore'],
    requirements=open("requirements.txt").readlines(),
    data_files=[('templates',['templates/score.html'])],
)