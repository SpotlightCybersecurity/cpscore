#!/bin/bash

set -e

echo "** Create volume"
docker volume create mysrc
echo "** copy source to volume"
tar c cpscore.py templates/score.html setup.py | docker run -i --rm -v mysrc:/src alpine tar xv -C /src
echo "** build windows"
docker run --rm -v mysrc:/src cdrx/pyinstaller-windows:python3 "pyinstaller --add-data 'templates/score.html;templates' -y --clean --onefile cpscore.py"
echo "** build linux"
docker run --rm -v mysrc:/src cdrx/pyinstaller-linux:python3 "pyinstaller --add-data 'templates/score.html:templates' -y --clean --onefile cpscore.py"
echo "** extract artifacts"
docker run -i --rm -v mysrc:/src alpine tar c -C /src dist  | tar xv
echo "** delete volume"
docker volume rm mysrc
echo "** done"
